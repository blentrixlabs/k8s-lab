#!/bin/bash -x
echo "This script is written to work with Ubuntu 16.04"

sudo echo "10.0.1.50 k8s-master" >> /etc/hosts
sudo echo "10.0.1.51 k8s-wn1" >> /etc/hosts
sudo echo "10.0.1.52 k8s-wn2" >> /etc/hosts

APISERVER_IP="10.0.1.50"
KUBEADM_VERSION="1.14.2-00"
KUBELET_VERSION="1.14.2-00"
KUBECTL_VERSION="1.14.2-00"
ADMIN_USER="vagrant"
ADMIN_GROUP="vagrant"
ADMIN_HOME="/home/vagrant"

sleep 3
echo
echo "Disable swap until next reboot"
echo 
sudo swapoff -a

echo "Update the local node"
sudo apt-get update && sudo apt-get upgrade -y
echo
echo "Install Docker"
sleep 3

sudo apt-get install -y docker.io
echo
echo "Install kubeadm and kubectl"
sleep 3

sudo sh -c "echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' >> /etc/apt/sources.list.d/kubernetes.list"
sudo sh -c "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -"
sudo apt-get update
sudo apt-get install -y kubeadm=${KUBEADM_VERSION} kubelet=${KUBELET_VERSION} kubectl=${KUBECTL_VERSION}

echo "Bootstrap a minimum viable Kubernetes cluster with kubeadm 'apiserver-advertise-address' is ${APISERVER_IP}"

sleep 3
sudo kubeadm init --pod-network-cidr 192.168.0.0/16 --apiserver-advertise-address=${APISERVER_IP}

sleep 5
echo "Running the steps explained at the end of the init output for you"

mkdir -p ${ADMIN_HOME}/.kube
sleep 2

sudo cp -i /etc/kubernetes/admin.conf ${ADMIN_HOME}/.kube/config
sleep 2

sudo chown ${ADMIN_USER}:${ADMIN_GROUP} ${ADMIN_HOME}/.kube/config

#echo
#echo "Installed - now to get Calico Project network plugin"

## If you are going to use a different plugin you'll want
## to use a different IP address, found in that plugins 
## readme file. 

#/bin/su -s /bin/bash -c 'wget https://goo.gl/eWLkzb -O /home/vagrant/calico.yaml' vagrant
#/bin/su -s /bin/bash -c 'kubectl apply -f /home/vagrant/calico.yaml' vagrant
#echo
#echo
#sleep 3
#echo "You should see this node in the output below"
#echo "It can take up to a mintue for node to show Ready status"
#echo
#sleep 20
#/bin/su -s /bin/bash -c 'kubectl get nodes' vagrant
#echo
#/bin/su -s /bin/bash -c 'kubectl get pods --all-namespaces' vagrant
#sleep 6


### Weave plugin

export kubever=$(/bin/su -s /bin/bash -c 'kubectl version' vagrant | base64 | tr -d '\n')
/bin/su -s /bin/bash -c 'kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"' vagrant
echo "You should see this node in the output below"
echo "It can take up to a mintue for node to show Ready status"
echo

sleep 20
/bin/su -s /bin/bash -c 'kubectl get nodes' vagrant
echo
/bin/su -s /bin/bash -c 'kubectl get pods --all-namespaces' vagrant
sleep 6

echo "Script finished. Move to the next step"

# capturing the join token will use it with worker nodes to join the cluster
if [ -f /vagrant/join-cluster.sh ]; then
   rm /vagrant/join-cluster.sh
fi
sudo kubeadm token create --print-join-command > /vagrant/join-cluster.sh
chmod +x /vagrant/join-cluster.sh
