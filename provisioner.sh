#!/bin/bash -x

sudo echo "10.0.1.50 k8s-master" >> /etc/hosts
sudo echo "10.0.1.51 k8s-wn1" >> /etc/hosts
sudo echo "10.0.1.52 k8s-wn2" >> /etc/hosts

KUBEADM_VERSION="1.14.2-00"
KUBELET_VERSION="1.14.2-00"
KUBECTL_VERSION="1.14.2-00"

echo "  This script is written to work with Ubuntu 16.04"
echo
sleep 3
echo "  Disable swap until next reboot"
echo
sudo swapoff -a

echo "  Update the local node"
sleep 2
sudo apt-get update && sudo apt-get upgrade -y
echo
sleep 2

echo "  Install Docker"
sleep 3
sudo apt-get install -y docker.io

echo
echo "  Install kubeadm and kubectl"
sleep 2
sudo sh -c "echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' >> /etc/apt/sources.list.d/kubernetes.list"
sudo sh -c "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -"
sudo apt-get update
sudo apt-get install -y kubeadm=${KUBEADM_VERSION} kubelet=${KUBELET_VERSION} kubectl=${KUBECTL_VERSION}

echo  " Joining the K8s Cluster"
sleep 3
while [ ! -f /vagrant/join-cluster.sh ] ;
do
  sleep 2
  echo "Waiting for Master node Configuration to Joining the Cluster "
done

sudo /vagrant/join-cluster.sh

echo " Script finished."
